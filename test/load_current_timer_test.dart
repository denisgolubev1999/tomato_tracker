import 'package:flutter_test/flutter_test.dart';
import 'package:tomato_trecker/domain/timer/actions_timer.dart';
import 'package:tomato_trecker/presentation/home/events/handlers/load_current_timer.dart';
import 'package:tomato_trecker/presentation/home/state.dart';

import 'di.dart';

main() {

  test("$LoadCurrentTimerEventHandler returns the same state is timer is NOT active", () async {
    final timer = fakeTimerManager(isActive: false);
    final handler = LoadCurrentTimerEventHandler(timer);

    final stateBeforeHandlingEvent = HomeState.initialize();
    final actual = await handler.handle(stateBeforeHandlingEvent, LoadCurrentTimerEvent());

    expect(actual, equals(stateBeforeHandlingEvent));
  });

  test("$LoadCurrentTimerEventHandler loads right state if timer is active", () async {
    final currentDuration = Duration(minutes: 10);
    final timerData = TimerModel(currentDuration, FAKE_ACTION);
    final timer = fakeTimerManager(isActive: true, timerData: timerData);

    final handler = LoadCurrentTimerEventHandler(timer);
    final actual = await handler.handle(HomeState.initialize(), LoadCurrentTimerEvent());

    expect(actual.currentTimer, equals(currentDuration));
    expect(actual.selectedAction, equals(FAKE_ACTION));
  });

}