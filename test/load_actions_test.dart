import 'package:flutter_test/flutter_test.dart';
import 'package:tomato_trecker/presentation/home/events/handlers/load_actions.dart';
import 'package:tomato_trecker/presentation/home/state.dart';

import 'di.dart';

main () async {
  test("returns right state", () async {

    final actionsRepository = fakeActionsRepository();

    final handler = LoadActionsEventHandler(actionsRepository);
    final actual = await handler.handle(HomeState.initialize(), LoadActionsEvent());

    expect(actual.actions, equals(fakeActions));
  });

}