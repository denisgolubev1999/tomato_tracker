import 'package:mockito/mockito.dart';
import 'package:tomato_trecker/data/ActionsRepository.dart';
import 'package:tomato_trecker/domain/timer/actions_timer.dart';
import 'package:tomato_trecker/domain/models/action.dart';

class MockActionsRepository extends Mock implements ActionsRepository {}
class MockTimerManager extends Mock implements SimpleActionsTimer {}

final FAKE_ACTION = SimpleAction.create("title", "icon");

final fakeActions = [
  SimpleAction.create("Бег", "run"),
  SimpleAction.create("Работа", "work"),
  SimpleAction.create("Работа", "work"),
  SimpleAction.create("Работа", "work"),
  SimpleAction.create("Работа", "work"),
  SimpleAction.create("Работа", "work"),
];

MockActionsRepository fakeActionsRepository(){
  final repository = MockActionsRepository();
  when(repository.getAll()).thenAnswer((invocation) => Future.value(fakeActions));
  return repository;
}

// ignore: avoid_init_to_null
MockTimerManager fakeTimerManager({bool isActive = false, TimerModel timerData = null}){
  final mock = MockTimerManager();
  when(mock.isActive()).thenAnswer((_) => Future.value(isActive));
  when(mock.get()).thenAnswer((_) => Future.value(timerData));
  return mock;
}