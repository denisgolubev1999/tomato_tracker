import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:tomato_trecker/data/ActionsRepository.dart';
import 'package:tomato_trecker/data/ResultsRepository.dart';
import 'package:tomato_trecker/domain/timer/actions_timer.dart';
import 'package:tomato_trecker/domain/timer/background_actions_timer.dart';
import 'package:tomato_trecker/domain/timer/timer_fields.dart';
import 'package:tomato_trecker/domain/timer/timer_storage.dart';
import 'package:tomato_trecker/presentation/event.dart';
import 'package:tomato_trecker/presentation/home/bloc.dart';
import 'package:tomato_trecker/presentation/home/events/home_events.dart';
import 'package:tomato_trecker/presentation/home/state.dart';
import 'package:bloc/bloc.dart';

TimerFields _timerFields;
Future<TimerFields> getTimerFields() async {
  if(_timerFields == null) {
    _timerFields = TimerFields();
    await _timerFields.init();
  }
  return _timerFields;
}

TimerStorage _timerStorage;
Future<TimerStorage> getTimerStorage() async {
  if(_timerStorage == null) {
    final timerFields = await getTimerFields();
    _timerStorage = PrefsTimerStorage(actionsRepository, timerFields);
  }
  return _timerStorage;
}

ActionsTimer _actionsTimer;
Future<ActionsTimer> getActionsTimer() async {
  if(_actionsTimer == null) {
    final timerStorage = await getTimerStorage();
    _actionsTimer = BackgroundTimerManager(timerStorage);
  }
  return _actionsTimer;
}

final actionsRepository = ActionsRepositoryImpl();
final resultsDatabase = ResultsRepository();

Future<EventHandler> _homeEventsHandler() async {
  return HomeEventsHandler(actionsRepository, await getActionsTimer());
}

Future<Bloc<AppEvent, HomeState>> provideHomeBloc(BuildContext context) async {
  return HomeBloc(context, await _homeEventsHandler());
}
