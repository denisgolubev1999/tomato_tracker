import 'package:tomato_trecker/domain/models/results.dart';

import 'dart:async';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class ResultsRepository {

  static const DB_NAME = "results.db";
  static const TABLE_NAME = "results";

  static const CREATE_TABLE_QUERY = "CREATE TABLE $TABLE_NAME("
      "${Result.START_TIME} TEXT PRIMARY KEY,"
      "${Result.ACTION_TITLE} TEXT,"
      "${Result.DESTINATION_TIME} TEXT,"
      "${Result.TIMER_STOPPED} TEXT"
      ");";

  static Database _database;

  Future<Database> _getDatabase() async {
    if(_database == null)
      _database = await _createDatabase();
    return _database;
  }

  Future<Database> _createDatabase() async {
    return openDatabase(
        join(await getDatabasesPath(), DB_NAME),
        onCreate: (db, version) {
          return db.execute(CREATE_TABLE_QUERY);
        },
        version: 1
    );
  }

  Future addNewResult(Result result) async {
    final database = await _getDatabase();
    await database.insert(TABLE_NAME, result.toMap(), conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<List<Result>> results() async {
    final database = await _getDatabase();
    final List<Map<String, dynamic>> data = await database.query(TABLE_NAME);
    return data.map((m) {
      return Result.fromMap(m);
    }).toList();
  }

}
