import 'package:tomato_trecker/domain/models/action.dart';

abstract class ActionsRepository {

  Future<List<SimpleAction>> getAll();
  Future<SimpleAction> getMostPopular();
  Future<ActionModel> getActionWithTitle(String actionTitle);

}

class ActionsRepositoryImpl implements ActionsRepository {

  Future<List<SimpleAction>> getAll(){
    return Future.value([
      SimpleAction.create("Бег", "run"),
      SimpleAction.create("Работа", "work"),
      SimpleAction.create("Работа", "work"),
      SimpleAction.create("Работа", "work"),
      SimpleAction.create("Работа", "work"),
      SimpleAction.create("Работа", "work"),
    ]);
  }

  Future<SimpleAction> getMostPopular(){
    return Future.value(SimpleAction.create("Бег", "run"));
  }

  @override
  Future<ActionModel> getActionWithTitle(String actionTitle) async {
    return (await getAll()).firstWhere((item) {
      return item.getTitle() == actionTitle;
    });
  }

}