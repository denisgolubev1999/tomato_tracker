import 'package:flutter/cupertino.dart';

class Result {

  final String actionTitle;
  final String startTime;
  final String destinationTime;
  final String timerStoppedTime;

  static const ACTION_TITLE = "actionTitle";
  static const START_TIME = "startTime";
  static const DESTINATION_TIME = "destinationTime";
  static const TIMER_STOPPED = "timerStoppedTime";

  Result({@required this.actionTitle, @required this.startTime, @required this.destinationTime, @required this.timerStoppedTime});

  bool isSuccess() {
    final timerStopped = DateTime.parse(timerStoppedTime).millisecondsSinceEpoch;
    final destination = DateTime.parse(destinationTime).millisecondsSinceEpoch;
    return timerStopped >= destination;
  }

  Map<String, dynamic> toMap() {
    return {
      ACTION_TITLE: this.actionTitle,
      START_TIME: this.startTime,
      DESTINATION_TIME: this.destinationTime,
      TIMER_STOPPED: this.timerStoppedTime,
    };
  }

  factory Result.fromMap(Map<String, dynamic> map) {
    return new Result(
      actionTitle: map[ACTION_TITLE] as String,
      startTime: map[START_TIME] as String,
      destinationTime: map[DESTINATION_TIME] as String,
      timerStoppedTime: map[TIMER_STOPPED] as String,
    );
  }

}