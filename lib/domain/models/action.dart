class SimpleAction implements ActionModel {

  static const MAX_TITLE_LENGTH = 10;

  final String _title;
  final String _icon;

  const SimpleAction._internal(this._title, this._icon);

  factory SimpleAction.create(String title, String icon){

    if(title.length > MAX_TITLE_LENGTH)
      throw InvalidActionException("title must be less or equal $MAX_TITLE_LENGTH");

    return SimpleAction._internal(title, icon);
  }

  @override
  String getIcon() {
    return _icon;
  }

  @override
  String getTitle() {
    return _title;
  }

}

class InvalidActionException implements Exception {
  final String title;
  const InvalidActionException(this.title);

  @override
  String toString() {
    return 'InvalidActionException{$title}';
  }

}

class EmptyAction implements SpecialAction {

  @override
  String getIcon() => "/special/empty";

  @override
  String getTitle() => " ";

}

abstract class ActionModel {
  String getIcon();
  String getTitle();
}


///
/// Just a marker of action that cannot be uses as a regular action
/// */
abstract class SpecialAction implements ActionModel {

  factory SpecialAction.empty() => EmptyAction();

}