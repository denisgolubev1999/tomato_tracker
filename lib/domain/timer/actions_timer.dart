import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:tomato_trecker/data/ActionsRepository.dart';
import 'package:tomato_trecker/data/ResultsRepository.dart';
import 'package:tomato_trecker/di.dart';
import 'package:tomato_trecker/domain/models/action.dart';
import 'package:background_fetch/background_fetch.dart';
import 'package:tomato_trecker/domain/models/results.dart';
import 'package:tomato_trecker/domain/timer/timer_storage.dart';

abstract class ActionsTimer {

  void start(TimerModel timer);
  void stop();
  
  Future<bool> isActive();
  Future<TimerModel> get();
  
  void doOnChanged(Function(Duration) onChanged);
  void doOnComplete(Function onCompleted);
  
}

class SimpleActionsTimer implements ActionsTimer {

  Timer _currentTimer;

  final TimerStorage _timerStorage;

  SimpleActionsTimer(this._timerStorage) {
    _restoreTimerIfNeed();
  }

  void _restoreTimerIfNeed() async {
    if(await _timerStorage.isCompleted()) return;
    _startInternalTimer();
  }

  @override
  void start(TimerModel timer) {
    if(_currentTimer != null)
      stop();

    _timerStorage.saveCurrentTimerInSharedPrefs(timer);
    _startInternalTimer();
  }

  @override
  void stop() {
    _currentTimer.cancel();
    _timerStorage.clearTimerInPrefs();
  }



  @override
  Future<bool> isActive() async => !(await _timerStorage.isCompleted());

  void _startInternalTimer(){
    _currentTimer = Timer.periodic(const Duration(seconds: 1), (timer) async {
      _notify();
    });
  }

  @override
  Future<TimerModel> get() {
    return _timerStorage.get();
  }


  Function(Duration) _onChanged;
  Function() _onComplete;

  @override
  void doOnChanged(Function(Duration) onChanged) {
    this._onChanged = onChanged;
  }
  @override
  void doOnComplete(Function onCompleted) {
    this._onComplete = onCompleted;
  }

  void _notify() async {
    if (_onChanged != null) _onChanged(Duration(seconds: await _timerStorage.restTime()));

    if (await _timerStorage.isCompleted()) {
      _currentTimer.cancel();
//      await _saveResults();
      if (_onComplete != null) _onComplete();
    }
  }

//  Future _saveResults() async {
//
//    final results = await _resultsDatabase.results();
//    print(results);
//
//    final prefs = await SharedPreferences.getInstance();
//    final actionTitle = prefs.getString(ACTIVE_ACTION);
//
//    final startTime = prefs.getString(STARTED_TIMER_PREFS_KEY);
//    final destination = prefs.getString(DESTINATION_TIMER_PREFS_KEY);
//    final timerStopped = DateTime.now().toString();
//
//    final result = Result(
//      actionTitle: actionTitle,
//      startTime: startTime,
//      destinationTime: destination,
//      timerStoppedTime: timerStopped
//    );
//
//    await _resultsDatabase.addNewResult(result);
//  }

}

class TimerModel {
  final Duration duration;
  final ActionModel action;
  const TimerModel(this.duration, this.action);
}