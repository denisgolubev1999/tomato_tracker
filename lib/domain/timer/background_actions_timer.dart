import 'package:background_fetch/background_fetch.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tomato_trecker/data/ActionsRepository.dart';
import 'package:tomato_trecker/data/ResultsRepository.dart';
import 'package:tomato_trecker/domain/timer/actions_timer.dart';
import 'package:tomato_trecker/domain/models/action.dart';
import 'package:tomato_trecker/domain/timer/timer_storage.dart';

class BackgroundTimerManager extends SimpleActionsTimer {

  static const String TIMER_ID = "TIMER_ID";

  BackgroundTimerManager(TimerStorage timerStorage) : super(timerStorage);

  @override
  Future<void> start(TimerModel timer) async {

    final timerId = await BackgroundFetch.configure(BackgroundFetchConfig(
        minimumFetchInterval: timer.duration.inMinutes,
        stopOnTerminate: false,
        enableHeadless: false,
        requiresBatteryNotLow: false,
        requiresCharging: false,
        requiresStorageNotLow: false,
        requiresDeviceIdle: false,
        requiredNetworkType: NetworkType.NONE
    ), (String taskId) async {
      // This is the fetch-event callback.
      print("[BackgroundFetch] Event received $taskId");
      print("[BackgroundFetch] Event received $taskId");
      print("[BackgroundFetch] Event received $taskId");
      print("[BackgroundFetch] Event received $taskId");
      print("[BackgroundFetch] Event received $taskId");
      print("[BackgroundFetch] Event received $taskId");
      print("[BackgroundFetch] Event received $taskId");
      // IMPORTANT:  You must signal completion of your task or the OS can punish your app
      // for taking too long in the background.
      BackgroundFetch.finish(taskId);
    }).then((int status) {
      print('[BackgroundFetch] configure success: $status');
      print('[BackgroundFetch] configure success: $status');
      print('[BackgroundFetch] configure success: $status');
      print('[BackgroundFetch] configure success: $status');
      print('[BackgroundFetch] configure success: $status');
      print('[BackgroundFetch] configure success: $status');
      print('[BackgroundFetch] configure success: $status');
      print('[BackgroundFetch] configure success: $status');

    }).catchError((e) {
      print('[BackgroundFetch] configure ERROR: $e');
      print('[BackgroundFetch] configure ERROR: $e');
      print('[BackgroundFetch] configure ERROR: $e');
      print('[BackgroundFetch] configure ERROR: $e');
      print('[BackgroundFetch] configure ERROR: $e');
      print('[BackgroundFetch] configure ERROR: $e');
      print('[BackgroundFetch] configure ERROR: $e');
      print('[BackgroundFetch] configure ERROR: $e');
      print('[BackgroundFetch] configure ERROR: $e');
    });

    final prefs = await SharedPreferences.getInstance();
    prefs.setInt(TIMER_ID, timerId);

    super.start(timer);
  }

  @override
  void stop() async {

    final prefs = await SharedPreferences.getInstance();
    final timerId = prefs.getInt(TIMER_ID);
    prefs.setInt(TIMER_ID, -1);
    BackgroundFetch.finish(timerId.toString());

    super.stop();
  }

}