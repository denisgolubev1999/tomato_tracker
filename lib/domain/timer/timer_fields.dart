
import 'package:shared_preferences/shared_preferences.dart';

class TimerFields {

  static const STARTED_TIMER_PREFS_KEY = "timer_started";
  static const DESTINATION_TIMER_PREFS_KEY = "timer_destination";
  static const ACTIVE_ACTION = "active_action";

  SharedPreferences prefs;

  Future init() async {
    this.prefs = await SharedPreferences.getInstance();
  }

  String getStartedDatetime() => prefs.getString(STARTED_TIMER_PREFS_KEY);
  String getDestinationDatetime() => prefs.getString(DESTINATION_TIMER_PREFS_KEY);
  String getActiveActon() => prefs.getString(ACTIVE_ACTION);

  Future setStartedDatetime(String value) async {
    await prefs.setString(STARTED_TIMER_PREFS_KEY, value);
  }

  Future setDestinationDatetime(String value) async {
    await prefs.setString(DESTINATION_TIMER_PREFS_KEY, value);
  }

  Future setActiveActon(String value) async {
    await prefs.setString(ACTIVE_ACTION, value);
  }

}