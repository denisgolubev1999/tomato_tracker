import 'package:shared_preferences/shared_preferences.dart';
import 'package:tomato_trecker/data/ActionsRepository.dart';
import 'package:tomato_trecker/domain/timer/actions_timer.dart';

import 'timer_fields.dart';

abstract class TimerStorage {

  void saveCurrentTimerInSharedPrefs(TimerModel timer);
  void clearTimerInPrefs();
  Future<int> passedTime();
  Future<int> restTime();
  Future<bool> isCompleted();
  Future<TimerModel> get();

}

class PrefsTimerStorage implements TimerStorage {

  final ActionsRepository _actionsRepository;
  final TimerFields _fields;

  PrefsTimerStorage(this._actionsRepository, this._fields);

  @override
  void saveCurrentTimerInSharedPrefs(TimerModel timer) async {
    final now = DateTime.now();
    final destination = DateTime.now().add(timer.duration);

    await _fields.setStartedDatetime(now.toString());
    await _fields.setDestinationDatetime(destination.toString());
    await _fields.setActiveActon(timer.action.getTitle());
  }

  @override
  void clearTimerInPrefs() async {
    await _fields.setStartedDatetime("");
    await _fields.setDestinationDatetime("");
    await _fields.setActiveActon("");
  }

  @override
  Future<int> passedTime() async {
    final startedString = _fields.getStartedDatetime();
    if (startedString == null || startedString.isEmpty) return 0;

    final started = DateTime.parse(startedString);
    final now = DateTime.now();
    return (now.millisecondsSinceEpoch - started.millisecondsSinceEpoch) ~/ 1000;
  }

  @override
  Future<int> restTime() async {
    final destinationString = _fields.getDestinationDatetime();
    if (destinationString == null || destinationString.isEmpty) return 0;

    final destination = DateTime.parse(destinationString);
    final now = DateTime.now();
    return (destination.millisecondsSinceEpoch - now.millisecondsSinceEpoch) ~/ 1000;
  }

  @override
  Future<bool> isCompleted() async {
    final destinationString = _fields.getDestinationDatetime();
    if (destinationString == null || destinationString.isEmpty) return true;

    final destination = DateTime.parse(destinationString);
    final now = DateTime.now();
    return now.compareTo(destination) >= 0;
  }

  @override
  Future<TimerModel> get() async {
    final actionTitle = _fields.getActiveActon();
    final action = await _actionsRepository.getActionWithTitle(actionTitle);
    return TimerModel(Duration(seconds: await restTime()), action);
  }

}