import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tomato_trecker/di.dart';
import 'package:tomato_trecker/domain/models/action.dart';
import 'package:tomato_trecker/presentation/event.dart';
import 'package:tomato_trecker/presentation/home/bloc.dart';
import 'package:tomato_trecker/presentation/home/page.dart';
import 'package:bloc/bloc.dart';
import 'package:tomato_trecker/presentation/home/state.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
          backgroundColor: const Color(0xFFFBFCFF),
          body: FutureBuilder(
            future: provideHomeBloc(context),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return _home(snapshot.data, context);
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          )),
    );
  }

  Widget _home(Bloc<AppEvent, HomeState> homeBloc, BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      child: BlocProvider<HomeBloc>(
        builder: (context) => homeBloc,
        child: HomePage(),
      ),
    );
  }
}
