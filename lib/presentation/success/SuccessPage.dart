import 'package:flutter/cupertino.dart';

class SuccessPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _State();
  }

}

class _State extends State<SuccessPage> {

  @override
  Widget build(BuildContext context) {
    return Center(child: Text("Success"));
  }

}
