abstract class AppEvent {}

abstract class EventHandler<E extends AppEvent, T> {

  Future<T> handle(T currentState, E event);

}