import 'package:flutter/material.dart';

class DigitalTimeView extends StatelessWidget {

  final Duration duration;

  const DigitalTimeView({this.duration = const Duration(seconds: 0), Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      "${_hours()}:${_minutes()}:${_seconds()}",
      style: TextStyle(fontSize: 50),
    );
  }

  String format(int value) {
    if (value < 10) return "0$value";
    return value.toString();
  }

  String _hours() => format(duration.inHours);

  String _minutes() => format(duration.inMinutes % 60);

  String _seconds() => format(duration.inSeconds % 60);
}
