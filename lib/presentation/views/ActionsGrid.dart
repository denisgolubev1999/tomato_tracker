import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tomato_trecker/domain/models/action.dart';
import 'package:tomato_trecker/presentation/views/ActionView.dart';
import 'dart:math';

class ActionsGrid extends StatefulWidget {

  static const ITEM_EXTENT = ActionView.WIDTH * 1.2;
  static const MORE_ARROW_HEIGHT = 25.0;
  static const TOP_PADDING = 8.0;

  final List<SimpleAction> items;
  bool isFull;
  final double maxHeight;

  ActionsGrid(this.items, {this.isFull = false, this.maxHeight = 400, Key key})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<ActionsGrid> with SingleTickerProviderStateMixin {
  Animation<double> _heightAnimation;
  AnimationController _animationController;

  double _height = 0;
  double _panStart = 0;
  double _lastDragY = 0;

  GlobalKey _key = GlobalKey();

  ScrollController _scrollController;

  @override
  void initState() {
    _animationController = AnimationController(
        duration: const Duration(milliseconds: 700), vsync: this);

    _scrollController = ScrollController();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _setAppropriateSize();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onPanStart: (details) {
        _panStart = details.globalPosition.dy;
      },
      onPanUpdate: (details) {
        _handleDragging(details.globalPosition.dy);
      },
      onPanEnd: (details) {
        if (!_animationController.isAnimating) _animateToAppropriateSize();
      },
      child: Container(
        key: _key,
        height:
            _height + ActionsGrid.MORE_ARROW_HEIGHT + ActionsGrid.TOP_PADDING,
        padding: EdgeInsets.only(top: ActionsGrid.TOP_PADDING),
        child: Column(
          children: <Widget>[_grid(context), _more()],
        ),
      ),
    );
  }

  Widget _more() {
    return GestureDetector(
      onTap: () {
        widget.isFull = !widget.isFull;
        _animateToAppropriateSize();
      },
      child: Text("Full"),
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  void _animateToFull() {
    if (_animationController.isAnimating) return;
    widget.isFull = true;
    final curve = CurvedAnimation(parent: _animationController, curve: Curves.fastOutSlowIn);
    _heightAnimation = Tween<double>(begin: _height, end: _maxHeight()).animate(curve);
    _heightAnimation.addListener(() {
      setState(() {
        _height = _heightAnimation.value;
      });
    });

    _animationController.reset();
    _animationController.forward();
  }

  void _animateToShrinked() {
    if (_animationController.isAnimating) return;
    widget.isFull = false;

    _scrollController.animateTo(0, duration: const Duration(milliseconds: 1000), curve: Curves.ease)
    ..then((_) {
      final curve = CurvedAnimation(parent: _animationController, curve: Curves.fastOutSlowIn);
      _heightAnimation = Tween<double>(begin: _height, end: _minHeight()).animate(curve);
      _heightAnimation.addListener(() {
        setState(() {
          _height = _heightAnimation.value;
        });
      });

      _animationController.reset();
      _animationController.forward();
    });
  }

  Widget _grid(context) {

    ScrollPhysics physics;
    if(widget.isFull){
      physics = AlwaysScrollableScrollPhysics();
    } else {
      physics = NeverScrollableScrollPhysics();
    }

    return SizedBox(
      height: _height,
      child: GridView.count(
        controller: _scrollController,
        childAspectRatio: ActionView.WIDTH / ActionView.HEIGHT,
        crossAxisCount: _itemsInRow(),
        physics: physics,
        children: _buildChildren(),
      ),
    );
  }

  int _itemsInRow() {
    final screenWidth = MediaQuery.of(context).size.width;
    return (screenWidth / ActionsGrid.ITEM_EXTENT).round();
  }

  List<Widget> _buildChildren() {
    return widget.items.map((SimpleAction it) {
      return ActionView(it);
    }).toList();
  }

  double _minHeight() {
    return ActionView.HEIGHT +
        ActionsGrid.TOP_PADDING +
        ActionsGrid.MORE_ARROW_HEIGHT;
  }

  double _maxHeight() {
    final rowsCount = widget.items.length / _itemsInRow();
    final fitSize = rowsCount * ActionView.HEIGHT;
    final result = min(widget.maxHeight, fitSize);
    return result;
  }

  double _getPosition() {
    final RenderBox renderBox = _key.currentContext.findRenderObject();
    return renderBox.localToGlobal(Offset.zero).dy;
  }

  double _getHeight() {
    final RenderBox renderBox = _key.currentContext.findRenderObject();
    return renderBox.size.height;
  }

  void _handleDragging(double dy) {
    final differenceY = dy - _panStart;
    print(_getPosition() + _getHeight());
    print("dy  = $dy");
//    final differenceY = dy - _getPosition() + _getHeight();

    if (differenceY > ActionView.HEIGHT / 3) {
      if (!widget.isFull) {
        _animateToFull();
        print('Animate to full');
      }
    } else if (differenceY < 0 && differenceY.abs() > ActionView.HEIGHT / 3) {
      if (widget.isFull) {
        print('Animate to scrincked');
        _animateToShrinked();
      }
    }
//    else if (differenceY > 0) {
//      print('Set size > ');
//      if(!widget.isFull){
//        setState(() {
//          final height = _height + (dy - _lastDragY);
//          _height = max(min(height, 500), ActionView.HEIGHT);
//        });
//      }
//    } else if (differenceY < 0) {
//      print('Set size < ');
//      if(widget.isFull){
//        setState(() {
//          final height = _height - (_lastDragY - dy);
//          _height = max(height, ActionView.HEIGHT);
//        });
//      }
//    }
    _lastDragY = dy;
  }

  void _animateToAppropriateSize() {
    if (widget.isFull) {
      _animateToFull();
    } else {
      _animateToShrinked();
    }
  }

  void _setAppropriateSize() {
    if (widget.isFull)
      _height = _maxHeight();
    else
      _height = _minHeight();
  }
}
