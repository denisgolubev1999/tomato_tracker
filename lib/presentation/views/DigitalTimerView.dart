import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:tomato_trecker/presentation/views/DigitalTImeView.dart';

class DigitalTimerView extends StatefulWidget {

  final Duration duration;

  const DigitalTimerView(this.duration, {Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }

}

class _State extends State<DigitalTimerView>{

  Timer _timer;
  Duration restTime;

  @override
  void initState() {
    _startTimer();
    super.initState();
  }


  @override
  void dispose() {
    _timer.cancel();
    _timer = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DigitalTimeView(duration: restTime);
  }

  void _startTimer() {
    restTime = widget.duration;
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        restTime = Duration(seconds: restTime.inSeconds - 1);
      });
    });
  }

}