import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../MyColors.dart';

class AccentButton extends StatelessWidget {

  final String _text;
  final Function _onTap;
  final double height;
  final double width;

  const AccentButton(this._text, this._onTap, {Key key, this.height = 40, this.width = 150}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: MyColors.ACCENT_COLOR,
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
      child: InkWell(
        onTap: _onTap,
        child: SizedBox(
          height: height,
          width: width,
          child: Center(
              child: Text(
            _text,
            style: TextStyle(fontSize: 20, color: Colors.white),
          )),
        ),
      ),
    );
  }
}
