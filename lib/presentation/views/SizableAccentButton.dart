import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../MyColors.dart';

class SizableAccentButton extends StatelessWidget {

  final String _text;
  final Function _onTap;

  const SizableAccentButton(this._text, this._onTap, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: MyColors.ACCENT_COLOR,
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
      child: InkWell(
        onTap: _onTap,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: SizedBox(
            height: 40,
            child: Center(
                child: Text(
              _text,
              style: TextStyle(fontSize: 20, color: Colors.white),
            )),
          ),
        ),
      ),
    );
  }
}
