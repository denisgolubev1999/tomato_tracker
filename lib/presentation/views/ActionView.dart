import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tomato_trecker/domain/models/action.dart';

class ActionView extends StatelessWidget {

  static const WIDTH = 80.0;
  static const HEIGHT = 100.0;
  static const ICON_SIZE = 60.0;

  final double size;

  static double aspectRatio() {
    return ActionView.WIDTH / ActionView.HEIGHT;
  }

  final ActionModel action;

  const ActionView(this.action, {this.size = 1, Key key}) : super(key: key);

  double iconSize() => ICON_SIZE * size;
  double width() => WIDTH * size;
  double height() => HEIGHT * size;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width(),
      height: height(),
      child: Column(
        children: <Widget>[
          _circle(),
          SizedBox(height: 4.0 * size,),
          _text()
        ],
      ),
    );
  }

  Widget _circle(){
    return Container(
      height: width(),
      width: height(),
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.white,
          boxShadow: [
            BoxShadow(color: Colors.grey, offset: Offset(1, 2), blurRadius: 3)
          ]
      ),
      child: Center(
        child: _icon(),
      ),
    );
  }


  Widget _icon(){
    return Container(
      height: iconSize(),
      width: iconSize(),
      decoration: BoxDecoration(
        image: DecorationImage(image: AssetImage("assets/actions/${action.getIcon()}.png"), fit: BoxFit.fill,)
      ),
    );
  }

  Text _text(){
    return Text(
        action.getTitle(),
      textAlign: TextAlign.center,
      style: TextStyle(
        color: Colors.black54,
        fontSize: (12 * size).toDouble(),
      ),
      maxLines: 2,
    );
  }

}

class BigActionView extends StatelessWidget {

  final ActionModel actionModel;

  const BigActionView(this.actionModel, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ActionView(actionModel, size: 3);
  }


}