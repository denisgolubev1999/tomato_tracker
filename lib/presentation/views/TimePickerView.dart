import 'package:flutter/material.dart';
import 'package:tomato_trecker/presentation/views/DigitalTImeView.dart';
import 'package:tomato_trecker/presentation/views/TimeView.dart';

class TimePickerView extends StatefulWidget {

  final TimePickerController controller;
  final int maxDurationInMinutes;
  final int minDurationInMinutes;

  const TimePickerView(this.controller, {this.maxDurationInMinutes = 120, this.minDurationInMinutes = 1, Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }

}

class _State extends State<TimePickerView> {

  static const double _ARROW_SIZE = 35;
  static const double _ARROW_PADDING = 5;

  Duration _duration = Duration(minutes: 25);

  @override
  Widget build(BuildContext context) {
    _postPickedTime();

    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        _leftArrow(),
        SizedBox(
          width: 150,
          child: Center(
            child: TimeView(duration: _duration,),
          ),
        ),
        _rightArrow()
      ],
    );
  }

  Widget _leftArrow() {
    return InkWell(
      onTap: () {
        _changeTime(-5);
      },
      child: Padding(
        padding: const EdgeInsets.all(_ARROW_PADDING),
        child: Icon(Icons.arrow_back_ios, size: _ARROW_SIZE,),
      ),
    );
  }

  Widget _rightArrow() {
    return InkWell(
      onTap: () {
        _changeTime(5);
      },
      child: Padding(
        padding: EdgeInsets.all(_ARROW_PADDING),
        child: Icon(Icons.arrow_forward_ios, size: _ARROW_SIZE,),
      ),
    );
  }

  void _changeTime(int deltaMinutes){
    // for testing
    setState(() {
      _duration = Duration(minutes: 1);
      _postPickedTime();
    });
    return;



    final newValue = _duration.inMinutes + deltaMinutes;
    if(newValue > widget.maxDurationInMinutes || newValue < widget.minDurationInMinutes) return;
    setState(() {
      _duration = Duration(minutes: newValue);
      _postPickedTime();
    });
  }

  _postPickedTime(){
    widget.controller._postDuration(_duration);
  }

}

class TimePickerController {

  Duration _duration;

  void _postDuration(Duration duration){
    this._duration = duration;
  }

  Duration getTime() => _duration;

}