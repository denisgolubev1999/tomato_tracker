import 'package:flutter/material.dart';

class TimeView extends StatelessWidget {

  final Duration duration;

  const TimeView({this.duration = const Duration(seconds: 0), Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      "${formatedValue()}",
      style: TextStyle(fontSize: 40),
    );
  }

  String formatedValue() {

    String formatUnit(int value, String name, int divider){
      return value % divider > 0 ? "${value % divider}$name " : "";
    }

    return formatUnit(duration.inHours, "h", 24) + formatUnit(duration.inMinutes, "m", 60) + formatUnit(duration.inSeconds, "s", 60);
  }
}
