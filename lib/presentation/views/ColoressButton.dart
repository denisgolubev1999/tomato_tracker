import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../MyColors.dart';

class ColoressButton extends StatelessWidget {

  final String _text;
  final Function _onTap;

  const ColoressButton(this._text, this._onTap, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
      child: InkWell(
        onTap: _onTap,
        child: SizedBox(
          height: 40,
          width: 150,
          child: Center(
              child: Text(
            _text,
            style: TextStyle(fontSize: 20, color: Colors.black54, decoration: TextDecoration.underline),
          )),
        ),
      ),
    );
  }
}
