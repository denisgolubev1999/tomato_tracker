import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tomato_trecker/domain/models/action.dart';
import 'package:tomato_trecker/presentation/views/ActionsContainer.dart';

class ActionsFullDialogContainer extends StatelessWidget {

  final List<SimpleAction> items;
  final Function(ActionModel) onSelected;

  const ActionsFullDialogContainer(this.items, this.onSelected, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _dialogContent();
  }

  Widget _dialogContent() {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: Container(
        color: Colors.green,
        child: ActionsContainer(
          items, onSelected, rows: ActionsContainer.MAX_ROWS,
        ),
      ),
    );
  }



}