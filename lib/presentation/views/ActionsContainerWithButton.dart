import 'package:flutter/cupertino.dart';
import 'package:tomato_trecker/domain/models/action.dart';
import 'package:tomato_trecker/presentation/views/ActionsContainer.dart';

class ActionsContainerWithButton extends StatelessWidget {

  final List<ActionModel> items;
  final Function(ActionModel) onSelected;

  const ActionsContainerWithButton(this.items, this.onSelected, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ActionsContainer(items, onSelected),
        _button()
      ],
    );
  }

  Widget _button() {
    return Align(
      alignment: Alignment.centerRight,
      child: GestureDetector(
        onTap: () {

        }, child: Text("All")
      ),
    );
  }

}