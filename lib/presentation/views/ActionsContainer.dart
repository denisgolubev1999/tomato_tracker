//import 'package:flutter/cupertino.dart';
//import 'package:tomato_trecker/domain/models/action.dart';
//import 'package:tomato_trecker/presentation/views/ActionView.dart';
//import 'dart:math';
//
//class ActionsContainer extends StatelessWidget {
//
//  final int rows;
//  final List<ActionModel> items;
//  final Function(ActionModel) onSelected;
//
//  const ActionsContainer(this.items, this.onSelected, {this.rows = 1, Key key})
//      : super(key: key);
//
//  static const MAX_ROWS = -1;
//  static const MARGIN = 8.0;
//
//  @override
//  Widget build(BuildContext context) {
//
//    return LayoutBuilder(
//      builder: (BuildContext context, BoxConstraints constraints) {
//        return SizedBox(
//          height: _height(constraints),
//          child: GridView.count(
//            childAspectRatio: ActionView.aspectRatio(),
//            crossAxisCount: _countInRow(constraints),
//            children: _itemViews(constraints),
//            mainAxisSpacing: MARGIN,
//            crossAxisSpacing: MARGIN,
//              physics: rows == MAX_ROWS ? AlwaysScrollableScrollPhysics() : NeverScrollableScrollPhysics(),
//          ),
//        );
//      },
//    );
//  }
//
//  double _height(BoxConstraints constraints){
//    return _maxViewedItem(constraints) / _countInRow(constraints) * ActionView.HEIGHT;
//  }
//
//  int _countInRow(BoxConstraints constraints){
//    return constraints.maxWidth ~/ (ActionView.WIDTH + MARGIN);
//  }
//
//  int _maxViewedItem(BoxConstraints constraints){
//    if(rows == MAX_ROWS) return items.length;
//    return _countInRow(constraints) * rows;
//  }
//
//  List<Widget> _itemViews(BoxConstraints constraints) {
//    final end = min(_maxViewedItem(constraints), items.length);
//    return items
//    .sublist(0, end)
//        .map((item) {
//      return GestureDetector(
//        onTap: () {
//          onSelected(item);
//        },
//        child: ActionView(item),
//      );
//    }).toList();
//  }
//
//}

import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:tomato_trecker/domain/models/action.dart';
import 'package:tomato_trecker/presentation/views/ActionView.dart';
import 'dart:math';

class ActionsContainer extends StatelessWidget {
  final int rows;
  final List<ActionModel> items;
  final Function(ActionModel) onSelected;

  const ActionsContainer(this.items, this.onSelected, {this.rows = 1, Key key})
      : super(key: key);

  static const MAX_ROWS = -1;
  static const MARGIN = 8.0;
  static const ACTION_SCALE = 0.8;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return Column(
          children: _rows(constraints),
        );
      },
    );
  }

  List<Row> _rows(BoxConstraints constraints) {
    final List<Row> list = [];
    for (int i = 0; i < rows; i++) {
      final Row row = _row(i, constraints);
      if (row != null) list.add(row);
    }
    return list;
  }

  double _height(BoxConstraints constraints) {
    return _maxViewedItem(constraints) /
        _countInRow(constraints) *
        ActionView.HEIGHT;
  }

  int _countInRow(BoxConstraints constraints) {
    return constraints.maxWidth ~/ (ActionView.WIDTH * ACTION_SCALE + MARGIN);
  }

  int _maxViewedItem(BoxConstraints constraints) {
    if (rows == MAX_ROWS) return items.length;
    return _countInRow(constraints) * rows;
  }

  List<Widget> _itemViews(BoxConstraints constraints) {
    final end = min(_maxViewedItem(constraints), items.length);
    return items.sublist(0, end).map((item) {
      return GestureDetector(
        onTap: () {
          onSelected(item);
        },
        child: ActionView(item),
      );
    }).toList();
  }

  Row _row(int i, BoxConstraints constraints) {
    final countInRow = _countInRow(constraints);
    final startIndex = i * countInRow;
    if (startIndex >= items.length) return null;

    final endIndex = min(startIndex + countInRow, items.length - 1);
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: items.sublist(startIndex, endIndex).map((item) {
        return GestureDetector(
          onTap: () {
            onSelected(item);
          },
          child: ActionView(
            item,
            size: ACTION_SCALE,
          ),
        );
      }).toList(),
    );
  }
}
