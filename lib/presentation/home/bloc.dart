import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tomato_trecker/data/ActionsRepository.dart';
import 'package:tomato_trecker/di.dart';
import 'package:tomato_trecker/domain/models/action.dart';
import 'package:tomato_trecker/presentation/event.dart';
import 'package:tomato_trecker/presentation/home/events/home_events.dart';
import 'package:tomato_trecker/presentation/home/state.dart';
import 'package:tomato_trecker/presentation/success/SuccessPage.dart';

import 'package:bloc/bloc.dart';

import 'events/handlers/load_actions.dart';

class HomeBloc extends Bloc<AppEvent, HomeState> {

  final BuildContext _context;
  final HomeEventsHandler _handler;
  HomeBloc(this._context, this._handler);

  @override
  HomeState get initialState {
    return HomeState.initialize();
  }

  @override
  Stream<HomeState> mapEventToState(AppEvent event) async* {

    (await getActionsTimer()).doOnChanged((passedTime) {
      print("passed $passedTime");
    });
    (await getActionsTimer()).doOnComplete(() {
      goToSuccessPage();
    });

    yield await _handler.handle(currentState, event);
  }

  void goToSuccessPage() {
    Navigator.push(_context, MaterialPageRoute(builder: (c) => SuccessPage()));
  }

}
