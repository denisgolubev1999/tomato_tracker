import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tomato_trecker/di.dart';
import 'package:tomato_trecker/presentation/home/bloc.dart';
import 'events/home_events.dart';
import 'package:tomato_trecker/presentation/home/pages/TimerActivePage.dart';
import 'package:tomato_trecker/presentation/home/pages/TimerStarterPage.dart';
import 'package:tomato_trecker/presentation/home/state.dart';
import 'package:tomato_trecker/presentation/views/TimePickerView.dart';

class HomePage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _HomePage();
  }

}

class _HomePage extends State<HomePage> {

  HomeBloc _bloc;

  @override
  void initState() {
    _bloc = BlocProvider.of<HomeBloc>(context);
    _bloc.dispatch(LoadActionsEvent());
    _bloc.dispatch(LoadCurrentTimerEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return BlocBuilder<HomeBloc, HomeState>(
      builder: (BuildContext context, HomeState state) {
        if(state.currentTimer == null)
          return TimerStarterPage(_bloc, state);
        else
          return TimerActivePage(_bloc, state);
      },
    );

  }

}
