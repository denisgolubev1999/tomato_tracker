library home_events;

import 'package:tomato_trecker/data/ActionsRepository.dart';
import 'package:tomato_trecker/domain/timer/actions_timer.dart';
import 'package:tomato_trecker/presentation/event.dart';
import 'package:tomato_trecker/presentation/home/state.dart';

import 'handlers/give_up.dart';
import 'handlers/load_actions.dart';
import 'handlers/load_current_timer.dart';
import 'handlers/select_action.dart';
import 'handlers/start_timer.dart';

export 'handlers/give_up.dart';
export 'handlers/load_actions.dart';
export 'handlers/load_current_timer.dart';
export 'handlers/select_action.dart';
export 'handlers/start_timer.dart';

class HomeEventsHandler extends EventHandler<AppEvent, HomeState> {

  final ActionsRepository _actionsRepository;
  final SimpleActionsTimer _timerManager;

  HomeEventsHandler(this._actionsRepository, this._timerManager);

  @override
  Future<HomeState> handle(HomeState currentState, AppEvent event) async {
    EventHandler handler;
    switch (event.runtimeType) {
      case LoadActionsEvent:
        handler = LoadActionsEventHandler(_actionsRepository);
        break;
      case LoadCurrentTimerEvent:
        handler = LoadCurrentTimerEventHandler(_timerManager);
        break;
      case SelectActionEvent:
        handler = SelectActionEventHandler();
        break;
      case StartTimerEvent:
        handler = StartTimerEventHandler(_timerManager);
        break;
      case GiveUpEvent:
        handler = GiveUpEventHandler(_timerManager);
        break;
    }

    if(handler == null)
      return currentState;

    return handler.handle(currentState, event);
  }

}