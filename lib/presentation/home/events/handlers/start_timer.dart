import 'package:tomato_trecker/domain/timer/actions_timer.dart';
import 'package:tomato_trecker/domain/models/action.dart';
import 'package:tomato_trecker/presentation/event.dart';
import 'package:tomato_trecker/presentation/home/state.dart';

class StartTimerEvent implements AppEvent {
  final Duration duration;
  StartTimerEvent(this.duration);
}

class StartTimerEventHandler implements EventHandler<StartTimerEvent, HomeState> {

  final SimpleActionsTimer _timerManager;

  const StartTimerEventHandler(this._timerManager);

  @override
  Future<HomeState> handle(HomeState currentState, StartTimerEvent event) async {
    if (currentState.selectedAction is SpecialAction)
      return Future.value(currentState);

    final newTimer = TimerModel(event.duration, currentState.selectedAction);
    _timerManager.start(newTimer);

    return currentState.setTimer(event.duration);
  }

}