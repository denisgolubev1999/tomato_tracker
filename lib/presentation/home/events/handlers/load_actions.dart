import 'package:tomato_trecker/data/ActionsRepository.dart';
import 'package:tomato_trecker/presentation/event.dart';
import 'package:tomato_trecker/presentation/home/state.dart';

class LoadActionsEvent implements AppEvent {}

class LoadActionsEventHandler implements EventHandler<LoadActionsEvent, HomeState> {

  final ActionsRepository _actionsRepository;

  const LoadActionsEventHandler(this._actionsRepository);

  @override
  Future<HomeState> handle(HomeState currentState, LoadActionsEvent even) async {
    final actions = await _actionsRepository.getAll();
    return currentState.copyWith(actions: actions);
  }

}