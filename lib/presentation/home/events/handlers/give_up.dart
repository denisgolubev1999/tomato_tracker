import 'package:tomato_trecker/domain/timer/actions_timer.dart';
import 'package:tomato_trecker/presentation/event.dart';
import 'package:tomato_trecker/presentation/home/state.dart';

class GiveUpEvent implements AppEvent {}

class GiveUpEventHandler implements EventHandler<GiveUpEvent, HomeState> {

  final SimpleActionsTimer _timerManager;

  const GiveUpEventHandler(this._timerManager);

  @override
  Future<HomeState> handle(HomeState currentState, GiveUpEvent event) async {
    // TODO: take away bonuses
    _timerManager.stop();
    return currentState.resetTimer();
  }

}