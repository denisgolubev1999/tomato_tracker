import 'package:tomato_trecker/domain/models/action.dart';
import 'package:tomato_trecker/presentation/event.dart';
import 'package:tomato_trecker/presentation/home/state.dart';

class SelectActionEvent implements AppEvent {
  final ActionModel action;
  SelectActionEvent(this.action);
}

class SelectActionEventHandler implements EventHandler<SelectActionEvent, HomeState> {

  @override
  Future<HomeState> handle(HomeState currentState, SelectActionEvent event) async {
    return currentState.copyWith(selectedAction: event.action);
  }

}