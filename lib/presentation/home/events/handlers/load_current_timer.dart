import 'package:tomato_trecker/domain/timer/actions_timer.dart';
import 'package:tomato_trecker/presentation/home/state.dart';

import '../../../event.dart';

class LoadCurrentTimerEvent implements AppEvent {}

class LoadCurrentTimerEventHandler
    implements EventHandler<LoadCurrentTimerEvent, HomeState> {
  final SimpleActionsTimer _timerManager;

  const LoadCurrentTimerEventHandler(this._timerManager);

  @override
  Future<HomeState> handle(
      HomeState currentState, LoadCurrentTimerEvent even) async {
    if (!(await _timerManager.isActive())) return currentState;

    final currentTimer = await _timerManager.get();

    final action = currentState.setTimer(currentTimer.duration);
    return action.copyWith(selectedAction: currentTimer.action);

//    return action;
  }
}
