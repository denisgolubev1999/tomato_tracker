import 'package:tomato_trecker/domain/models/action.dart';

class HomeState {

  final ActionModel selectedAction;
  final Duration currentTimer;
  final List<ActionModel> actions;

  HomeState({this.selectedAction, this.actions, this.currentTimer});

  factory HomeState.initialize() {
    return HomeState(
      selectedAction: SpecialAction.empty(),
      actions: []
    );
  }

  HomeState copyWith({
    ActionModel selectedAction,
    List<ActionModel> actions,
  }) {
    return new HomeState(
      selectedAction: selectedAction ?? this.selectedAction,
      actions: actions ?? this.actions,
      currentTimer: this.currentTimer
    );
  }

  HomeState resetTimer() {
    return new HomeState(
      selectedAction: this.selectedAction,
      actions: this.actions,
    );
  }

  HomeState setTimer(Duration duration) {
    return new HomeState(
      selectedAction: this.selectedAction,
      actions: this.actions,
      currentTimer: duration
    );
  }



}