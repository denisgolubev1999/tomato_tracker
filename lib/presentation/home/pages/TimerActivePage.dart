
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:tomato_trecker/presentation/home/bloc.dart';
import 'package:tomato_trecker/presentation/home/pages/GiveUpDialog.dart';
import 'package:tomato_trecker/presentation/home/state.dart';
import 'package:tomato_trecker/presentation/views/AccentButton.dart';
import 'package:tomato_trecker/presentation/views/ActionView.dart';
import 'package:tomato_trecker/presentation/views/ColoressButton.dart';
import 'package:tomato_trecker/presentation/views/DigitalTimerView.dart';

class TimerActivePage extends StatelessWidget {

  final HomeBloc _bloc;
  final HomeState _state;

  const TimerActivePage(this._bloc, this._state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Center(
          child: BigActionView(_state.selectedAction),
        ),
        SizedBox(height: 40,),
        Center(
          child: DigitalTimerView(_state.currentTimer),
        ),
        SizedBox(height: 100,),
        Center(
          child: ColoressButton("GIVE UP", () {
            _onClickGiveUp(context);
//            final pickedDuration = _timePickerController.getTime();
//            timerManager.newTimer(pickedDuration, _state.selectedAction.getTitle());
          }),
        )
      ],
    );
  }

  void _onClickGiveUp(BuildContext context){
    showDialog(context: context, builder: (context) {
      return GiveUpDialog(_bloc);
    });
  }

}