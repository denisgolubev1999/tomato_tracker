
import 'package:flutter/material.dart';
import 'package:tomato_trecker/domain/models/action.dart';
import 'package:tomato_trecker/presentation/home/events/home_events.dart';
import 'package:tomato_trecker/presentation/views/AccentButton.dart';
import 'package:tomato_trecker/presentation/views/ActionView.dart';
import 'package:tomato_trecker/presentation/views/ActionsContainer.dart';
import 'package:tomato_trecker/presentation/views/TimePickerView.dart';

import '../../../di.dart';
import '../bloc.dart';
import '../state.dart';

class TimerStarterPage extends StatelessWidget {

  final HomeBloc _bloc;
  final HomeState _state;

  TimePickerController _timePickerController;

  TimerStarterPage(this._bloc, this._state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    _timePickerController = TimePickerController();

    return Column(
      children: <Widget>[
        SizedBox(height: 16,),
        ActionsContainer(_state.actions, _onItemSelected, rows: 1,),
        SizedBox(height: 40,),
        Center(
          child: BigActionView(_state.selectedAction),
        ),
        SizedBox(height: 40,),
        Center(
          child: TimePickerView(_timePickerController),
        ),
        SizedBox(height: 60,),
        Center(
          child: AccentButton("START", _onClickStart, width: 200, height: 50,),
        )
      ],
    );
  }

  void _onItemSelected(ActionModel action){
    _bloc.dispatch(SelectActionEvent(action));
  }

  void _onClickStart(){
    final pickedTime = _timePickerController.getTime();
    _bloc.dispatch(StartTimerEvent(pickedTime));
  }


}