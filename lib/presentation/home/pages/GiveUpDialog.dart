
import 'package:flutter/material.dart';
import 'package:tomato_trecker/presentation/home/bloc.dart';
import 'package:tomato_trecker/presentation/home/events/home_events.dart';
import 'package:tomato_trecker/presentation/views/AccentButton.dart';
import 'package:tomato_trecker/presentation/views/ColoressButton.dart';
import 'package:tomato_trecker/presentation/views/SizableAccentButton.dart';

class GiveUpDialog extends StatelessWidget {

  final HomeBloc _bloc;

  const GiveUpDialog(this._bloc, {Key key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 3,
      child: Container(
        width: 300,
        height: 200,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        child: Column(
          children: <Widget>[
            _title(),
            _buttons(context)
          ],
        ),
      ),
    );
  }

  Widget _title() => Padding(
    padding: const EdgeInsets.symmetric(vertical: 32, horizontal: 16),
    child: Text("Do you really want to Give Up?", style: TextStyle(fontSize: 22), textAlign: TextAlign.center,),
  );

  Widget _buttons(BuildContext context) => Row(
    mainAxisAlignment: MainAxisAlignment.spaceAround,
    children: <Widget>[
      ColoressButton("I want to Give Up", () {
        _bloc.dispatch(GiveUpEvent());
        _closeDialog(context);
      }),
      SizableAccentButton("NO", () {
        _closeDialog(context);
      })
    ],
  );

  void _closeDialog(BuildContext context){
    Navigator.of(context, rootNavigator: true).pop();
  }

}